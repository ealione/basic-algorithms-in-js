## About

[![Build Status](https://travis-ci.org/mgechev/javascript-algorithms.svg?branch=Jakehp-patch-1)](https://travis-ci.org/mgechev/javascript-algorithms)

This repository contains JavaScript implementations of different famous Computer Science algorithms.

API reference with usage examples available
<a href="https://mgechev.github.io/javascript-algorithms/" target="_blank">here</a>.

## Development

**To install all dev dependencies**

Call:

```bash
npm install
```

**To setup repository with documentation**

```bash
npm run doc
```

This will build the documentation and open it in your browser.

**To update .html files with documentation**

Just run `npm run doc` again.

**To run tests**

Call:

```bash
gulp test
```

and all `*.spec.js` files will be executed.

**To deploy documentation site**

```bash
npm run deploy
```

This requires you to have commit access to your Git remote.
